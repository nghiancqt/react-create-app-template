import { combineReducers } from 'redux'
import { i18nState } from 'redux-i18n'
import accountsReducer from '../containers/accounts/reducers'

const rootReducer = combineReducers({
  accountsReducer,
  i18nState
})

export default rootReducer
