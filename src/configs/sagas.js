import 'regenerator-runtime/runtime' // eslint-disable-line
import { all } from 'redux-saga/effects'

import WatchAccounts from '../containers/accounts/sagas'

export default function* rootSagas() {
  yield all([
    WatchAccounts()
  ])
}
