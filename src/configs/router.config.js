import * as React from 'react'
import PropTypes from 'prop-types'
import { withRouter, matchPath, Redirect } from 'react-router-dom'

import PrivateRoute from '../components/router/privateRouter'
import PublicRoute from '../components/router/publicRouter'
import { PrivateLayout, PublicLayout } from '../components/layout'

import routes from '../routes'

class AppRoutes extends React.Component {
  static propTypes = {
    history: PropTypes.object,
    location: PropTypes.object
  }

  static defaultProps = {
    location: {},
    history: {}
  }

  render() {
    const { history, location } = this.props
    const portal = 'pharmacy'
    let privateRoute = null
    let publicRoute = null
    let match = null

    for (let i = 0; i < routes.length; i++) {
      match = matchPath(location.pathname, {
        path: routes[i].path,
        exact: true,
        sensitive: routes[i].component.sensitive
      })
      if (match) {
        if (routes[i].private) {
          privateRoute = routes[i]
        } else {
          publicRoute = routes[i]
        }
      }
    }

    if (privateRoute && (!privateRoute.portal || privateRoute.portal.includes(portal))) {
      return (
        <PrivateLayout history={history}>
          <PrivateRoute
            component={privateRoute.component}
            exact
            path={privateRoute.path}
            key="private"
            authed
          />
        </PrivateLayout>
      )
    }
    if (publicRoute && (!publicRoute.portal || publicRoute.portal.includes(portal))) {
      return (
        <PublicLayout history={history}>
          <PublicRoute
            authed
            component={publicRoute.component}
            exact
            path={publicRoute.path}
            key="public"
          />
        </PublicLayout>
      )
    }

    return <Redirect to="/404" />
  }
}
/*eslint-disable */
export default withRouter(AppRoutes);
/* eslint-enable */
// export default AppRoutes;
