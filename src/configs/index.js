import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'
import { persistStore, createTransform } from 'redux-persist'
import CryptoJS from 'crypto-js'
import I18n from 'redux-i18n'
import { createBrowserHistory } from 'history'

import store from './store.config'
import * as serviceWorker from '../serviceWorker'
import translations from '../locales/translations'
import AppRoutes from './router.config'

import '../App.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { loading: true }
  }

  componentWillMount() {
    const encryptTransform = createTransform(
      (inboundState) => {
        const encrypted = CryptoJS.AES.encrypt(JSON.stringify(inboundState), 'UI2D4yfiIb4teTPE')
        return encrypted.toString()
      },
      (outboundState) => {
        const decrypted = CryptoJS.AES.decrypt(outboundState, 'UI2D4yfiIb4teTPE')
        return JSON.parse(decrypted.toString(CryptoJS.enc.Utf8))
      },
      { whitelist: ['accountsReducer', 'i18nState'] }
    )

    persistStore(store, {
      transforms: [encryptTransform],
      whitelist: ['accountsReducer', 'i18nState']
    }, () => {
      this.setState({ loading: false })
    })
    window.portal = window.location.hostname.split('.')[0] // eslint-disable-line
  }

  render() {
    const history = createBrowserHistory({ basename: '/' })
    const { loading } = this.state

    if (loading === true) {
      return <div style={{ textAlign: 'center' }}>Initial application...</div>
    }
    return (
      <Provider store={store}>
        <I18n translations={translations}>
          <Router history={history}>
            <AppRoutes />
          </Router>
        </I18n>
      </Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
