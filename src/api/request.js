import axios from 'axios'
import store from '../configs/store.config'

const Request = {
  callAPI(method, url, args) {
    return new Promise(async(resolve, reject) => {
      try {
        let accessToken = ''
        const { accountsReducer: { userInfo }, i18nState: { lang } } = store.getState()  // eslint-disable-line
        if (args && args.accessToken) {
          accessToken = args.accessToken // eslint-disable-line
          delete args.accessToken
        } else {
          accessToken = userInfo && userInfo.accessToken ? userInfo.accessToken : ''
        }
        const response = await axios({
          method,
          url,
          data: args,
          headers: {
            Authorization: accessToken
            // 'Access-Control-Allow-Origin': '*'
            // Language: lang
          }
        })
        if (process.env.NODE_ENV === 'development') {
          console.log('===SUCCESS===', url, response)
        }

        resolve({ data: response.data, status: response.status })
      } catch (error) {
        if (process.env.NODE_ENV === 'development') {
          console.log(error.message)
        }

        if (error) console.log(error)
        reject(error)
      }
    })
  }
}

export default Request
