const API_BASE_URL = 'https://api.loprx.com'
const version = 'v1'

export {
  API_BASE_URL,
  version
}
