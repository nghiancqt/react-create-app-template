import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Footer from './footer'
import Header from './header'
import Sidebar from './sidebar'
import Loading from '../loading'

class PrivateLayout extends Component {
  static propTypes = {
    history: PropTypes.object,
    children: PropTypes.any,
    dispatch: PropTypes.func
    // path: PropTypes.string
  }

  constructor(prop) {
    super(prop)
    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {
    const { userInfo, history } = this.props
    if (userInfo.accessToken) {
      this.setState({ isLoading: false }) // eslint-disable-line
    } else {
      history.push('/sign-in')
    }
  }

  render() {
    const {
      history, children, dispatch, userInfo
    } = this.props
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <div class="m-grid m-grid--hor m-grid--root m-page">
          <div class="m-content" style={{ textAlign: 'center', padding: 30 }}>Waitting...</div>
        </div>
      )
    }
    return (
      <div class="m-grid m-grid--hor m-grid--root m-page">
        <Loading />
        <Header history={history} dispatch={dispatch} userInfo={userInfo} />
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
          <button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn"> {/* eslint-disable-line */}
            <i class="la la-close" />
          </button>
          <Sidebar userInfo={userInfo} history={history} location={history.location} />
          {children}
        </div>
        <Footer />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    userInfo: state.accountsReducer.userInfo
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivateLayout)
