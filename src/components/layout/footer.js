import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Footer extends Component {
  render() {
    return (
      <footer class="m-grid__item m-footer">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
          <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
              <span class="m-footer__copyright">
              2017 &copy; LOPRX
              </span>
            </div>
            <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
              <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                <li class="m-nav__item">
                  <Link to="/about" class="m-nav__link">
                    <span class="m-nav__link-text">
                    About
                    </span>
                  </Link>
                </li>
                <li class="m-nav__item">
                  <Link to="/privacy" class="m-nav__link">
                    <span class="m-nav__link-text">
                    Privacy
                    </span>
                  </Link>
                </li>
                <li class="m-nav__item">
                  <a href="#" class="m-nav__link">
                    <span class="m-nav__link-text">
                    T&C
                    </span>
                  </a>
                </li>
                <li class="m-nav__item m-nav__item">
                  <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                    <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
