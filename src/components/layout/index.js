import PrivateLayout from './privateLayout'
import PublicLayout from './publicLayout'

export {
  PrivateLayout,
  PublicLayout
}
