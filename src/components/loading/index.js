import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

class Loading extends Component {
  static propTypes = {
    commonReducer: PropTypes.object
  }

  static defaultProps = {
    commonReducer: {}
  }

  render() {
    const { commonReducer } = this.props
    if (commonReducer && !commonReducer.isShowLoading) {
      return null
    }
    return (
      <div
        style={{
          position: 'fixed',
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(88, 103, 228, .4)',
          zIndex: 1051
        }}
      >
        <div
          class="m-loader m-loader--lg"
          style={{
            width: 30, position: 'absolute', top: 0, bottom: 0, marginLeft: '50%'
          }}
        />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    commonReducer: state.commonReducer
  }
}

export default connect(mapStateToProps)(Loading)
