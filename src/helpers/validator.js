function checkEmail(email) {
  const formatEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return formatEmail.test(email)
}

function checkPassword(password) {
  // const formatPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d/!/@/#/$/%/^/&/*/(/)/=/_/+/?]{6,}$/
  // return formatPass.test(password)
  return password.length > 5
}

export {
  checkEmail,
  checkPassword
}
