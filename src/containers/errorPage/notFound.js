import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class NotFound extends Component {
  render() {
    return (
      <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid  m-error-1">
          <div class="m-error_container">
            <span class="m-error_number">
              <h1>404</h1>
            </span>
            <p class="m-error_desc">
              <span>OOPS! Something went wrong here. </span>
              <p><Link to="/" className="m-link m-login__account-link"> GO BACK</Link></p>
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export default NotFound
