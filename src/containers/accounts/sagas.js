import {
  call, put, takeLatest, all
} from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import * as types from './types'
import { request, API_BASE_URL } from '../../api'

function* registerAsync(action) {
  try {
    yield put({ type: 'SHOW_LOADING' })
    const { code, data } = yield call(request, 'POST', API_BASE_URL.REGISTER_URL, action.payload)
    yield put({ type: 'HIDE_LOADING' })
    switch (code) {
      case responseCode.REQUEST_SUCCESS: {
        yield put({ type: types.REGISTER_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.REGISTER_FAIL })
      }
    }
  } catch (error) {
    yield [
      put({ type: 'HIDE_LOADING' }),
      put({ type: types.REGISTER_FAIL })
    ]
  }
}

export default function* watchAction() {
  yield all([
    takeLatest(types.REGISTER_ASYNC, registerAsync)
  ])
}
