import * as types from './types'

const initialState = {
  userInfo: {}
}

export default function accounts(state = initialState, action) {
  switch (action.type) {
    case types.REGISTER_SUCCESS:
      return {
        ...state,
        userInfo: action.payload
      }

    default:
      return state
  }
}
