import Loadable from 'react-loadable'
import Loading from '../components/loading'

const LoginInPage = Loadable({
  loader: () => import('../containers/accounts/login/loginContainer'),
  loading: Loading
})
const HomePage = Loadable({
  loader: () => import('../containers/home/homeContainer'),
  loading: Loading
})

const NotFoundPage = Loadable({
  loader: () => import('../containers/errorPage/notFound'),
  loading: Loading
})

export default [
  {
    path: '/sign-in',
    component: LoginInPage
  },
  {
    path: '/',
    component: HomePage,
    private: true
  },
  {
    path: '/404',
    component: NotFoundPage
  }
]
